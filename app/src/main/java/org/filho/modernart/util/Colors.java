package org.filho.modernart.util;

/**
 * Created by filho on 30/04/17.
 */

public class Colors {
    public static String toHex(int color) {
        return String.format("#%06X", (0xFFFFFF & color));
    }
}
