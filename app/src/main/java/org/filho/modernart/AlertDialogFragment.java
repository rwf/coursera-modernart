package org.filho.modernart;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

// Class that creates the AlertDialog
public class AlertDialogFragment extends DialogFragment {

		public static AlertDialogFragment newInstance() {
			return new AlertDialogFragment();
		}

		// Build AlertDialog using AlertDialog.Builder
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return new AlertDialog.Builder(getActivity())
					.setMessage("Inspired by the works of art in exhibit at the MOMA. Click below to learn more.")
					
					// User cannot dismiss dialog by hitting back button
					.setCancelable(false)
					
					// Set up No Button
					.setNegativeButton("Not now",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,	int id) {
									// Just close the dialog
									dialog.dismiss();
								}
							})
							
					// Set up Yes Button
					.setPositiveButton("Visit MOMA",
							new DialogInterface.OnClickListener() {
								public void onClick(final DialogInterface dialog, int id) {
									// Open web page https://www.moma.org/collection/works/78987
									Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.moma.org/collection/works/78987"));
									startActivity(intent);
								}
							}).create();
		}
	}